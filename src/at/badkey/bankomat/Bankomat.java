package at.badkey.bankomat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Bankomat {
	
	public static boolean c = true;
	public static int kontostand = 0;
	
	public static void main(String[] args) throws IOException {
		while(c){
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.println("Selektieren Sie die gewünschte Funktion:");
			System.out.println("1. Einzahlen");
			System.out.println("2. Auszahlen");
			System.out.println("3. Kontostand");
			System.out.println("4. Ende");
			
			String input = reader.readLine();
			
			switch(input) {
				case "1":
					System.out.println("Welchen betrag möchten Sie einzahlen?");
					String betrag = reader.readLine();
					kontostand += Integer.parseInt(betrag);
					System.out.println("Sie haben "+kontostand+" eingezahlt!");
				break;
				case "2":
					System.out.println("Welchen betrag möchten Sie auszahlen?");
					String betrag1 = reader.readLine();
					if(Integer.parseInt(betrag1) > kontostand) {
						System.out.println("Ihr Kontostand ist zu klein für diese Auszahlung!");
					}else {
						kontostand -= Integer.parseInt(betrag1);
						System.out.println("Sie haben "+betrag1+" ausgezahlt!");
					}
				break;
				case "3":
					System.out.println("Sie haben einen Kontostand von "+kontostand);
				break;
				case "4":
					System.out.println("Auf Wiedersehen!");
					c = false;
				break;
			}
		}
	}

}
